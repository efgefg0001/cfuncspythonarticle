ffi.cdef("""
// линейные программы
double normal_distr_density(double x, double mu, double sigma, unsigned int *memory);
double squared_x(double x, unsigned int *memory);
double sin_degrees(double degrees, unsigned int *memory);
int func(int x);
// программы с ветвлением
const char *solve_square_equation(double a, double b, double c, unsigned int *memory);
// цикл с условием
double sum_of_infinite_series(double x, double epsilon, unsigned int *memory);
// цикл с заданным числом итераций
double factorial(unsigned int number, unsigned int *memory);
unsigned int get_max_len_incr_series(double *arr, unsigned int count, unsigned int *memory);
// вложенные циклы
double get_abs_dif_max_min(double *matr, int nrows, int ncolumns, unsigned int *memory);
void multipl_table(unsigned int *memory);
// работа со строками
const char *get_first_longest_word(char *str, unsigned int *memory);
// файлы
const int BUF_SIZE = 1001;
typedef enum {ERROR_OPEN_READ=1, ERROR_OPEN_WRITE} file_errors;
int copy_strs(const char *input, const char *output, unsigned int start, unsigned int end, unsigned int *memory);
// подпрограммы
const int MAX_D = 256;
double compute_rpn(char *str, unsigned int *memory);
// рекурсия
int compute_fib(int number);
""")
